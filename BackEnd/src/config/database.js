const sql = require('mssql');

const config = {
  user: 'ucr',
  password: '12345',
  server: 'localhost\\SQLEXPRESS',
  database: 'website',
  options: {
    encrypt: true,
    enableArithAbort: true
  }
};

exports.config = config;
exports.sql = sql;
