import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import { Router } from '@angular/router';

import { ApiService } from '../api/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user = {
    correo: "",
    nombre: "",
    pass: ""
  }

  constructor(private api: ApiService, private router: Router){}

  ngOnInit(): void {
  }

  login(){
    this.user.pass = btoa(this.user.pass); //Funcion para encriptar en base64.

    this.api.login(this.user).subscribe(
      (res:any) => {
        console.log(res);
        if(res != null)
          this.router.navigate([`/user/${res[0].nombre}`]);
      }
    );
  }

}
